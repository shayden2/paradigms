var submitButton = document.getElementById("bsr-submit-button");
submitButton.onmouseup = makeNetworkCallToUserApi;
var resetButton = document.getElementById("bsr-clear-button");
resetButton.onmouseup = clearData;

function makeNetworkCallToUserApi() {
    var xhr = new XMLHttpRequest();
    var url = "https://randomuser.me/api/";
    xhr.open("GET", url, true);

    xhr.onload = function (e) {
        updateUserTextWithResponse(xhr.responseText);
    }

    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function updateUserTextWithResponse(response_text) {
    var response_json = JSON.parse(response_text);
    var header1 = document.getElementById("header-line1");
    var label1 = document.getElementById("response-line1");
    var label2 = document.getElementById("response-line2");
    var label3 = document.getElementById("response-line3");

    var response = response_json['results'][0];
    var name = response['name'];
    var age = response['registered']['age'];
    var phone = response['phone']

    header1.innerHTML = "Random User:";

    if (response['name']['first'] == null) {
        label1.innerHTML = "Random User could not be generated.";
    }
    else {
        label1.innerHTML = name['title'] + '. ' + name['first'] + ' ' + name['last'];
        label2.innerHTML = 'Age: ' + age;
        label3.innerHTML = ' Phone: ' + phone;
        var firstname = name['first'];
        makeNetworkCallToNationalityApi(firstname);
    }
}

function makeNetworkCallToNationalityApi(name) {
    var xhr = new XMLHttpRequest();
    var url = "https://api.nationalize.io/?name=" + name;
    xhr.open("GET", url, true);

    xhr.onload = function (e) {
        makeNetworkCallToCountryApi(xhr.responseText);
    }

    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function makeNetworkCallToCountryApi(response_text) {
    var xhr = new XMLHttpRequest();
    response = JSON.parse(response_text);

    if (response['country'][0] == null) {
        makeNetworkCallToUserApi();
        return;
    }

    code = response['country'][0]['country_id'];
    var url = "https://restcountries.eu/rest/v2/alpha/" + code;
    xhr.open("GET", url, true);

    xhr.onload = function (e) {
        updateNationalityTextWithResponse(xhr.responseText);
    }

    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function updateNationalityTextWithResponse(response_text) {
    var label4 = document.getElementById("response-line4");
    response = JSON.parse(response_text);
    if (response['name'] == null) {
        label4.innerHTML = "Country: United States of America";
    }
    else {
        label4.innerHTML = "Country: " + response['name'];
    }
}

function clearData() {
    var label1 = document.getElementById("response-line1");
    var label2 = document.getElementById("response-line2");
    var label3 = document.getElementById("response-line3");
    var label4 = document.getElementById("response-line4");
    var header1 = document.getElementById("header-line1");
    label1.innerHTML = "";
    label2.innerHTML = "";
    label3.innerHTML = "";
    label4.innerHTML = "";
    header1.innerHTML = "";
}
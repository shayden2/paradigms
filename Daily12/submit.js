var submitButton = document.getElementById('submitButton');
submitButton.onmouseup = getFormInfo;

function getFormInfo() {
    // get form values
    var team1name = document.getElementById("team1name").value;
    var team1score = parseInt(document.getElementById("team1score").value);
    var team2name = document.getElementById("team2name").value;
    var team2score = parseInt(document.getElementById("team2score").value);
    var sport = document.getElementById("sport-select").value;

    // populate dictionary with form values
    headline_dict = {};
    headline_dict['team1name'] = team1name;
    headline_dict['team1score'] = team1score;
    headline_dict['team2name'] = team2name;
    headline_dict['team2score'] = team2score;
    headline_dict['sport'] = sport;

    // call function to display headline
    displayHeadline(headline_dict);
}

function displayHeadline(headline) {
    // get headline body
    var headlineBody = document.getElementById("headlineBody");

    // build headline string
    if (headline['team1score'] > headline['team2score']) {
        headline['string'] = headline['sport'] + ': ' + headline['team1name'] + ' beat ' + headline['team2name'] + ' with a final score of ' + headline['team1score'] + '-' + headline['team2score'] + '.';
    }
    else if (headline['team1score'] < headline['team2score']) {
        headline['string'] = headline['sport'] + ': ' + headline['team2name'] + ' beat ' + headline['team1name'] + ' with a final score of ' + headline['team2score'] + '-' + headline['team1score'] + '.';
    }
    else {
        headline['string'] = headline['sport'] + ': ' + headline['team1name'] + ' ties ' + headline['team2name'] + ' with a final score of ' + headline['team1score'] + '-' + headline['team2score'] + '.';
    }

    // display headline string
    headlineBody.innerHTML = headline['string'];
}
var submitButton = document.getElementById("send-button");
var clearButton = document.getElementById("clear-button");

submitButton.onmouseup = getFormInfo;
clearButton.onmouseup = resetForm;

function getFormInfo() {
    var answerLabel1 = document.getElementById("answer-label1");
    var answerLabel2 = document.getElementById("answer-label2");
    answerLabel1.innerHTML = "";
    answerLabel2.innerHTML = "";

    var machineName = document.getElementById("select-server-address").value;
    var portNumber = document.getElementById("input-port-number").value;
    var HTTPTypeGET = document.getElementById("radio-get").checked;
    var HTTPTypePUT = document.getElementById("radio-put").checked;
    var HTTPTypePOST = document.getElementById("radio-post").checked;
    var HTTPTypeDELETE = document.getElementById("radio-delete").checked;
    var inputKey = document.getElementById("input-key").value;
    var useKey = document.getElementById("checkbox-use-key").checked;
    var useBody = document.getElementById("checkbox-use-message").checked;
    var messageBody = document.getElementById("text-message-body").value;

    var options = {};
    options["machineName"] = machineName;
    options["portNumber"] = portNumber;
    options["inputKey"] = inputKey;
    options["messageBody"] = messageBody;
    options["useKey"] = useKey;
    options["useBody"] = useBody;
    if (HTTPTypeGET == true) {
        options["HTTPType"] = "GET";
    }
    else if (HTTPTypePUT == true) {
        options["HTTPType"] = "PUT";
    }
    else if (HTTPTypePOST == true) {
        options["HTTPType"] = "POST";
    }
    else if (HTTPTypeDELETE == true) {
        options["HTTPType"] = "DELETE";
    }

    if (options["portNumber"] == "") {
        var responseLabel = document.getElementById("answer-label1");
        responseLabel.innerHTML = "Invalid options";
        return;
    }
    if (options["useKey"] && (options["inputKey"] == "")) {
        var responseLabel = document.getElementById("answer-label1");
        responseLabel.innerHTML = "Invalid options";
        return;
    }
    if (options["useBody"] && (options["messageBody"] == "")) {
        var responseLabel = document.getElementById("answer-label1");
        responseLabel.innerHTML = "Invalid options";
        return;
    }
    sendRequest(options);
}

function sendRequest(options) {
    var xhr = new XMLHttpRequest();
    var url = options["machineName"] + ":" + options["portNumber"] + "/movies/";
    if (options["useKey"]) {
        url = url + options["inputKey"];
    }

    xhr.open(options["HTTPType"], url, true);

    xhr.onload = function (e) {
        updateResponseText(xhr.responseText, options["HTTPType"]);
    }

    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    }

    if (options["useBody"]) {
        xhr.send(options["messageBody"]);
    }
    else {
        xhr.send(null);
    }
}

function updateResponseText(responseText, HTTPType) {
    console.log(responseText);
    var response = JSON.parse(responseText);
    var label1 = document.getElementById("answer-label1");
    label1.innerHTML = responseText;

    if (HTTPType == "GET") {
        if (response["title"] != null && response["genres"] != null) {
            var label2 = document.getElementById("answer-label2");
            label2.innerHTML = response["title"] + " belongs to the genres: " + response["genres"];
        }
    }
}

function resetForm() {
    var portNumber = document.getElementById("input-port-number");
    var inputKey = document.getElementById("input-key");
    var messageBody = document.getElementById("text-message-body");
    var answerLabel1 = document.getElementById("answer-label1");
    var answerLabel2 = document.getElementById("answer-label2");

    portNumber.value = "";
    inputKey.value = "";
    messageBody.value = "";
    answerLabel1.innerHTML = "";
    answerLabel2.innerHTML = "";
}